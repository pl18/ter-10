package service

import (
	"context"

	. "gitlab.com/pl18/ter-10/api"
)

// Service type
//  Helloworld Service

type HelloWorldService struct {
	UnimplementedHelloWorldServiceServer
}

// Create a new service
func NewHelloWorldService() *HelloWorldService {
	service := &HelloWorldService{}
	return service
}

//  Sends a greeting
func (service *HelloWorldService) SayHello(ctx context.Context, request *SayHelloRequest) (*SayHelloResponse, error) {
	return &SayHelloResponse{}, nil
}
